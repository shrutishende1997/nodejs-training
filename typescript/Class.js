// class User {
//     name:string;
//     private age:number;
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var User = /** @class */ (function () {
    function User(name, age) {
        this.name = name;
        this.age = age;
    }
    return User;
}());
// using inheritance
var Admin = /** @class */ (function (_super) {
    __extends(Admin, _super);
    function Admin(name, age, permissions) {
        var _this = _super.call(this, name, age) || this;
        _this.permissions = permissions;
        return _this;
    }
    return Admin;
}(User));
//using interface
// interface CalculationContainer {
//     res:number,
//     print():void
// }
// type  CalculatiossnResult = CalculationContainer[];
var user = new User('Max', 30);
console.log(user.name);
function add(a, b) {
    return a + b;
}
// const result = add('5','2')
// console.log(result); //terminal error: Argument of type 'string' is not assignable to parameter of type 'number'
//const result = add(5,2);
//console.log(result);  // //output 7
//example 2
//below example we add HTMLInputElement we can say the way we represent is type casting because tyeescript doesnt have idea about the element is which type so we need to specify which type is
var num1 = document.getElementById('num1');
var num2 = document.getElementById('num2');
var buttonElement = document.querySelector('button');
var OutputMode;
(function (OutputMode) {
    OutputMode[OutputMode["CONSOLE"] = 0] = "CONSOLE";
    OutputMode[OutputMode["ALERT"] = 1] = "ALERT";
})(OutputMode || (OutputMode = {}));
function printResult(result, printMode) {
    if (printMode === OutputMode.CONSOLE) {
        console.log(result);
    }
    if (printMode === OutputMode.ALERT) {
        alert(result);
    }
}
var results = [];
buttonElement.addEventListener('click', function () {
    var numberInput1 = +num1.value;
    var numberInput2 = +num2.value;
    var result = add(numberInput1, numberInput2);
    var resultContainer = {
        res: result,
        print: function () {
            console.log("chk", this.res);
        }
    };
    results.push(resultContainer);
    printResult(result, OutputMode.ALERT);
});
//generic type
function logAndEcho(val) {
    console.log(val);
    return val;
}
logAndEcho('Hi THERE').split('');
