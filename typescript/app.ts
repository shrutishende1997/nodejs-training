function add(a: number, b: number){
    return a + b;
}

// const result = add('5','2')
// console.log(result); //terminal error: Argument of type 'string' is not assignable to parameter of type 'number'


//const result = add(5,2);
//console.log(result);  // //output 7

//example 2
//below example we add HTMLInputElement we can say the way we represent is type casting because tyeescript doesnt have idea about the element is which type so we need to specify which type is
const num1 = document.getElementById('num1') as HTMLInputElement; 
const num2 = document.getElementById('num2') as HTMLInputElement; 
const buttonElement = document.querySelector('button');
type printMode = 'console' | 'alert'; // this is union we can use both property
enum OutputMode {CONSOLE,ALERT}
function printResult(result,printMode: OutputMode) {

    if(printMode === OutputMode.CONSOLE){
        console.log(result);
   }
    if(printMode === OutputMode.ALERT){
       alert(result);
   }
}
//type is keyword it  means you define your own type
type CalculationResult = {res:number, print:() => void}[];
let results : CalculationResult  = [];

buttonElement.addEventListener('click',()=>{
    const numberInput1 = +num1.value;
    const numberInput2 = +num2.value;
    const result = add(numberInput1,numberInput2);
    const resultContainer = {
        res:result,
        print() {
            console.log("chk",this.res);
        }
    };
    results.push(resultContainer);
    printResult(result,OutputMode.ALERT);
  
})  
