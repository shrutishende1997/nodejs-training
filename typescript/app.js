function add(a, b) {
    return a + b;
}
// const result = add('5','2')
// console.log(result); //terminal error: Argument of type 'string' is not assignable to parameter of type 'number'
//const result = add(5,2);
//console.log(result);  // //output 7
//example 2
//below example we add HTMLInputElement we can say the way we represent is type casting because tyeescript doesnt have idea about the element is which type so we need to specify which type is
var num1 = document.getElementById('num1');
var num2 = document.getElementById('num2');
var buttonElement = document.querySelector('button');
var OutputMode;
(function (OutputMode) {
    OutputMode[OutputMode["CONSOLE"] = 0] = "CONSOLE";
    OutputMode[OutputMode["ALERT"] = 1] = "ALERT";
})(OutputMode || (OutputMode = {}));
function printResult(result, printMode) {
    if (printMode === OutputMode.CONSOLE) {
        console.log(result);
    }
    if (printMode === OutputMode.ALERT) {
        alert(result);
    }
}
var results = [];
buttonElement.addEventListener('click', function () {
    var numberInput1 = +num1.value;
    var numberInput2 = +num2.value;
    var result = add(numberInput1, numberInput2);
    var resultContainer = {
        res: result,
        print: function () {
            console.log("chk", this.res);
        }
    };
    results.push(resultContainer);
    printResult(result, OutputMode.ALERT);
});
