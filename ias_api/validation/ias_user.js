const Joi = require('@hapi/joi');

const authSchema = Joi.object({
    name:Joi.string().required(),
    email:Joi.string().required(),
    mobile:Joi.number().required(),    
    password:Joi.number().required(),    
    current_status:Joi.number().required(),    
    batch:Joi.number().required(),    

})

module.exports = {authSchema}

//https://www.geeksforgeeks.org/node-js-export-module/#:~:text=exports%20in%20Node.,js%20applications.