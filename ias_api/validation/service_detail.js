const Joi = require('@hapi/joi');

const ServiceDetailSchema = Joi.object({
    designation:Joi.string().required(),
    department_id:Joi.number().required(),
    is_additional_charge:Joi.number(),
    last_serving_department:Joi.string().required(),
    service_status:Joi.string().required(),
    start_date:Joi.required(),
    till_date:Joi.required(),
    address:Joi.required(),
    district_id:Joi.required(),
    other_details:Joi.required()
})