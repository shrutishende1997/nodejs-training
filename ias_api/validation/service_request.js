const Joi = require('@hapi/joi');

const ServiceSchema = Joi.object({
    designation: Joi.string().required(),
    status: Joi.string().required(),
    district_id: Joi.string().required(),
    department_id: Joi.string().required(),
    mobile: Joi.string().required(),
    email: Joi.string().required(),
    request_priority: Joi.string().required(),
    request_date: Joi.string().required(),
    due_date: Joi.string().required(),
    subject: Joi.string().required(),
    description: Joi.string().required(),
    application_status: Joi.string().required(),
    is_draft: Joi.string().required(),
   // ias_user_id: Joi.string().required(),
})
module.exports = {ServiceSchema}