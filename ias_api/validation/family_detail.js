const Joi =  require('@hapi/joi');

const FamilyDetailSchema = Joi.object({
    name: Joi.string().required(),
    middle_name: Joi.string(),
    last_name:Joi.string(),
    age:Joi.number().integer().min(1).max(100).required(),
    relation:Joi.number().required(),
    gender:Joi.number().required(),
    mobile_no:Joi.number().required(),
   // ias_user_id:Joi.required()

})
module.exports = {FamilyDetailSchema}