const jwt = require('jsonwebtoken');
require('dotenv').config();

const verifyToken = (req,res,next) =>{
    const token  = req.headers["x-access-token"];
    if(token){
        try{
            const decode = jwt.verify(token,'secret');
            req.user = decode;
           // res.status(200).json({status: "true", message:"valid Token.", data:decode })
            next();
        }catch(err){
            return res.json({
                status:true,
                message:err
            });
        }

    }else{
        return res.json({
            status:false,
            message:'Authatication fail'
        });
    }
  //  return next();
}

module.exports = verifyToken;