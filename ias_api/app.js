const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./Routes/ias.route');
require('dotenv').config();
const mongoose = require('mongoose');

const app = express();
const port = process.env.PORT;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/',routes);

mongoose.connect(process.env.URL).then(()=> console.log("connection succesfully..."))
.catch((err)=> console.log(err));

app.listen(port,()=>{
    console.log('app is running on port  '+port);
})


//using bodyParser we can send data in json format or encoded format
//That function will act as middleware.


//bodyParser
//express.static       //useful middleware they are by-default 
//morgon 
//session

//middleware  req,res,next


////Module exports are the instruction that tells Node.js which bits of code 
//(functions, objects, strings, etc.) to “export” 
//from a given file so other files are allowed to access the exported code



//https://www.geeksforgeeks.org/node-js-export-module/#:~:text=exports%20in%20Node.,js%20applications



