const express = require('express');
const router = express.Router();
const authController = require('../controller/AuthController')
const serviceController = require('../controller/ServiceRequest.Controller')
const familyController = require('../controller/FamilyDetail.Controller')
const iasUserDetailController = require('../controller/IasUserDetail.Controller')
const serviceDetailController = require('../controller/ServiceDetail.Controller');
const miscController = require('../controller/misc.controller');
const multer  = require('multer')
const upload = multer({ dest: 'uploads/' })

const auth = require('../Middleware/auth');
router.get('/test',authController.test);
router.post('/register',authController.register);
router.post('/otp',authController.otpVerification);
router.post('/login',authController.login)
router.get('/me',auth,authController.me)

//service-request
router.get('/:id/service-request',auth,serviceController.list)
router.delete('/:id/service-request',auth,serviceController.remove)
router.post('/service-request',auth,upload.array('document',10),serviceController.create)
router.post('/service-request-test',upload.array('document',10),serviceController.testservice)


//family detail
router.post('/family-detail',auth,familyController.create);
router.get('/:id/family-detail',auth,familyController.show);
router.delete('/:id/family-detail',auth,familyController.remove);
router.put('/:id/family-detail',auth,familyController.edit);

//basic-info
router.put('/basic-info',auth,iasUserDetailController.update)

//serviceDetail
router.post('/serviceDetail',auth,serviceDetailController.create);
router.get('/serviceDetail',auth,serviceDetailController.show);
router.put('/serviceDetail/:id',auth,serviceDetailController.edit);
router.delete('/serviceDetail/:id',auth,serviceDetailController.remove);

//misc
router.post('/department',miscController.createDepartment);
router.post('/gender',miscController.createGender);

module.exports = router;

//Module exports are the instruction that tells Node.js which bits of code 
//(functions, objects, strings, etc.) to “export” 
//from a given file so other files are allowed to access the exported code
////https://www.npmjs.com/package/multer