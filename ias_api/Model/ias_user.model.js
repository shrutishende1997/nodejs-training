const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSchema = new Schema({
    name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        trim:true,
        required:true,
        unique: true
    },
    mobile:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
   
  
    email_verified: {
        type:Date,
        default:''
    },
    admin_approve: {
        type: Boolean,
        default: false,
    },
    otp:{
        type: String,
        default: "",
    },
},
    {
        timestamps:true,
    }
);
module.exports = mongoose.model('users',UserSchema)


// Mongoose provides a straight-forward, schema-based 
// solution to model your application data. It includes
//  built-in type casting, validation, query building, business
//   logic hooks and more, out of the box.

