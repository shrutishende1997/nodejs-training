const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let departmentSchema = new Schema({
    name:{
        type:String,
        required:true
    }
},
{
    timestamps:true
}
)

module.exports = mongoose.model('departments',departmentSchema)
