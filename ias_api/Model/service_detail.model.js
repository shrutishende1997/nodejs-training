const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ServiceSchema = new Schema({
    designation:{
        type:String,
        required:true
    },
    department_id:{
        type:Number,
        required:true
    },
    is_additional_charge:{
        type:Number,
        required:true
    },
    last_serving_department:{
        type:String,
        required:true,
    },
    service_status:{
        type:Number,
        required:true
    },
    start_date:{
        type:Date,
        required:true
    },
    till_date:{
        type:Date,
        required:true
    },
    address:{
        type:String,
        required:true
    },
    district_id:{
        type:Number,
        required:true
    },
    other_details:{
        type:String,
        required:true
    },
    ias_user_id:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'user',
        trim:true
    }
},

{
    timestamps:true
}
)
module.exports = mongoose.model('ias_user_service_details',ServiceSchema)