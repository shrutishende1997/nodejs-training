const mongoose = require("mongoose");


const ServiceSchema = new mongoose.Schema({ 

    name:{
        type:String,
        require:true

    },
    ias_user_id :{
        type:mongoose.Schema.Types.ObjectId,
        ref:'user',
        trim:true
    }  
},
{
    timestamps:true
}

)

module.exports = mongoose.model("service_request_document", ServiceSchema)