const { number, string } = require('@hapi/joi');
const mongoose = require('mongoose');
const Schema = mongoose.Schema

let IasUserDetailSchema = new Schema({
    first_name:{
        type:String,
        required:true    
    },
    middle_name:{
        type:String,
       // required:true
    },
    last_name:{
        type:String,
      //  required:true
    },   
    dob:{
        type:Date,
       // required:true
    },
    gender_id:{
        type:Number,
      //  required:true
    },
    current_status:{
        type:Number,
       // required:true
    },
    batch:{
        type:Number,
        required:true
    },
    is_address_same:{
        type:Number,
       // required:true
    },
    temporary_address:{
        type:String,
       // required:true
    },
    temporary_district:{
        type:Number,
      //  required:true
    },
    permanent_address:{
        type:String,
       // required:true
    },
    permanent_disctict:{
        type:Number,
       // required:true
    },
    ias_user_id:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'user',
        trim:true
    }

});
module.exports = mongoose.model('ias_user_detail',IasUserDetailSchema);
