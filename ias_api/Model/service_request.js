const mongoose = require("mongoose");

const ServiceSchema = new mongoose.Schema({
    designation: {
        type: String,
        required: true,
        trim: true,
        maxlength: 25,
      },
     status : {
          type:String,
          required:true,
          trim:true,
        } ,
    district_id : {
        type:Number,
        required:true,
        trim:true
    } ,
    department_id : {
        type:Number,
        required:true,
        trim:true
    } ,
    mobile :{
        type:Number,
        required:true,
        trim:true
    },
    email:{
        type:String,
        required:true,
        trim:true
    },
    request_priority:{
        type:Number,
        required:true
    },
    request_date:{
        type:Date,
        required:true
    },

    due_date : {
        type:Date,
        required:true
    },
    subject : {
        required:true,
        type:String
    },
    description : {
        required:true,
        type:String
    },
    application_status :{
        required:true,
        type:Boolean
    },
    is_draft : {
        type:Boolean,
        required:true
    },
    
    ias_user_id :{
        type:mongoose.Schema.Types.ObjectId,
        ref:'user',
        trim:true
    }  

})

module.exports = mongoose.model("ias_service_requests", ServiceSchema)
  