const { string, number } = require('@hapi/joi');
const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let FamilySchema = new Schema({
    name:{
        type:String,
        required:true
    },
    middle_name : {
        type:String,
     },
    last_name :{
        type:String,
     },
    age : {
        type:Number,
        required:true
    } ,
    relation:{
        type:Number,
        required:true
    },
    gender:{
        type:Number,
        required:true,
       // comments:[{1:male,2:female,3:other}]
    },
    mobile_no:{
        type:Number,
       // required:true
    },
    ias_user_id:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'user',
        trim:true
    }
})
module.exports = mongoose.model('family_detail',FamilySchema)
