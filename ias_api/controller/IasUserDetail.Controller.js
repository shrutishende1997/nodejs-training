const IasUserDetail =  require('../Model/ias_user_detail');

exports.update = async function(req,res){
    console.log(req.user)
    
   IasUserDetail.findOneAndUpdate({ias_user_id:req.user.id},{$set:req.body},
    function(err,result){
       if(err){
        return res.json({
            status: false,
            message: "result detail updted fail",
            data:err
         })
       }
       return res.json({
          status: true,
          message: "result detail updted successfully",
          data:result
       })
    });
    
}