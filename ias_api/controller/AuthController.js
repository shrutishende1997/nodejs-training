const {authSchema} = require('../validation/ias_user');
const IasUser =  require('../Model/ias_user.model');
const bcrypt = require("bcrypt");
const { findOne } = require('../Model/ias_user.model');
const jwt = require('jsonwebtoken');
const IasUserDetail =  require('../Model/ias_user_detail');
exports.test = function(){
    console.log("hello");
}

exports.register = async function(req,res){
    const result =  authSchema.validate(req.body);
    if(result.error){
        return res.json({
            status:false,
            message: result.error.details[0].message
        })
    }
    let password = await bcrypt.hash(req.body.password,12);

    let user = new IasUser({
        name:req.body.name,
        email:req.body.email,
        mobile:req.body.mobile,
        password:password,
        otp: Math.random().toString(36).slice(2),
     })

    console.log("my user",user._id)
     let basic_info = new IasUserDetail({
        first_name:req.body.name,
        current_status:req.body.current_status,
        batch:req.body.batch,
        ias_user_id:user._id
    })

      basic_info.save();
     user.save(function(err){
         if(err){
             return res.json({
                 status:false,
                 message:err
             })
         }else{
             return res.json({
                 status:true,
                 message:'user register successfully',
                 data: user
             })
         }
     }) 
  
}

exports.otpVerification = async function(req,res){
   // console.log(req.body.email);
    if(req.body.email == null || req.body.otp == null){
        return res.json({
            status:false,
            message:'please fill all field'
        })
    }

  const user_exist = await IasUser.findOne({email:req.body.email});
    if(!user_exist){
        return res.json({
            status:false,
            message:'please check email'
        });
    }
     if(user_exist.otp === req.body.otp){
        
        IasUser.findOneAndUpdate({ email: user_exist.email }, { email_verified: Date(), }, async function (err, re) {
            if (err) {
                return res.json({
                    status:false,
                    message:err
                })
            }else{
                return res.json({
                    status:true,
                    message:'Otp Verified SuccessFully'
                })
            }
        
        });


    }else{
        return res.json({
            status:false,
            message:'please check otp'
        });
    }
    
   
}

exports.login = async function(req,res){
    if(req.body.email == null ||req.body.password == null ){
        return res.json({
            status:false,
            message:'Please Enter All field'
        });
    }

    const user_exist = await IasUser.findOne({email:req.body.email});
   
     if(!user_exist){
        return res.json({
            status:false,
            message:'Please Enter Valid Email'
         });
      }else if(user_exist.email_verified == null){
        return res.json({
            status:false,
            message:'Email Id is not Valid'
         });
      }else{
         const token = jwt.sign({username:user_exist.name, 
        role:{name:'user'},id:user_exist.id,email:user_exist.email}, 
        'secret', {expiresIn : '2h'}, process.env.JWT_SECRET_KEY);
            
        return  res.json({
            status:true,
            message:token,
            data:user_exist
        })

      }      
}   

exports.me = async function(req,res){

  return  res.json({
        status:true,
        message:'authatication successfully',
        data :req.user
    })
}