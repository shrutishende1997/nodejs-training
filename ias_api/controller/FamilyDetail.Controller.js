const {FamilyDetailSchema} = require('../validation/family_detail');
const Family = require('../Model/family_deatil');
var ObjectId = require("mongodb").ObjectId;
const { func } = require('@hapi/joi');
exports.create = async function(req,res){
    const result = FamilyDetailSchema.validate(req.body)
    if(result.error){
        return res.json({
            status:false,
            message:result.error.details[0].message
        })
    }
  //  console.log(req.user.id);
    let FamilyDetail = new Family({
        name:req.body.name,
        middle_name:req.body.middle_name,
        last_name:req.body.last_name,
        age:req.body.age,
        relation:req.body.relation,
        gender:req.body.gender,
        mobile_no:req.body.mobile_no,
        ias_user_id:req.user.id
    });
    FamilyDetail.save(function(err){
        if(err){
            return res.json({
                status:false,
                message:err
            })
        }
        return res.json({
            status:true,
            message:'Family detail added successfully',
            data:FamilyDetail
        })
    })
}
exports.show = async function(req,res){
     //console.log(req.params.id);
     const user_id = ObjectId(req.params.id);
     Family.find({ias_user_id:user_id}, function(err,result){
        if(err){
            return res.json({
                status:false,
                message:err
            })
        }
        return res.json({
            status:true,
            message:result
        })
     })

}

exports.remove = async function(req,res){
    //console.log(req.params.id);
    Family.findByIdAndRemove(req.params.id,function(err,result){
        if(err){
            return res.json({
                status:false,
                message:err
            })
        }else{
            return res.json({
                status:false,
                message:result
            })
        }

    })
}
exports.edit = function(req,res){
   Family.findByIdAndUpdate(req.params.id,{$set:req.body},
        function(err,family){
           if(err) return next(err);
           return res.json({
              status: true,
              message: "Family detail updted successfully",
              data:family
           })
        });
}