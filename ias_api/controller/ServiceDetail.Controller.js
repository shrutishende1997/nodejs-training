const mongoose = require('mongoose')
const ServiceDetailModel =  require('../Model/service_detail.model');

const ObjectId = require('mongodb').ObjectId;
module.exports = {
    create:function(req,res){
       let ServiceDetail = {
        designation:req.body.designation,
        department_id:req.body.department_id,
        is_additional_charge:req.body.is_additional_charge,
        last_serving_department:req.body.last_serving_department,
        service_status:req.body.service_status,
        start_date:req.body.start_date,
        till_date:req.body.till_date,
        address:req.body.address,
        district_id:req.body.district_id,
        other_details:req.body.other_details,
        ias_user_id:req.user.id
         };

         ServiceDetailModel.create(ServiceDetail,function(err,result){
                if(err){
                    return res.json({
                        status:false,
                        message:err
                    })
                }  
                return res.json({
                    status:true,
                    message:result
                })
            
         })
         //console.log(ServiceDetail);

    },

    show:function(req,res){
    //    console.log(req.user.id);
    ServiceDetailModel.find({ias_user_id:req.user.id},function(err,result){
        if(err){
            return res.json({
                status:false,
                message:err
            });
        }
        return res.json({
            status:true,
            message:result
        })

    })


    },

    edit:function(req,res){
      // console.log(req.body);
        ServiceDetailModel.findByIdAndUpdate(req.params.id,{$set:req.body},function(err,result){
            if(err){
                return res.json({
                    status:false,
                    message:err
                })
            }
            return res.json({
                status:true,
                message:'Service Detail Update Successfully',
                data:req.body
            })
        })
    },

    remove:function(req,res){
      //  console.log(req.params.id);
      ServiceDetailModel.findByIdAndRemove(req.params.id,function(err,result){
          if(err){
              return res.json({
                  status:false,
                  message:err
              })
          }
          return res.json({
            status:true,
            message:result
        })
      })
    }

}