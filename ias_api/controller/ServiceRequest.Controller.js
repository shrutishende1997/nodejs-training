const ServiceRequest = require('../Model/service_request');
const {ServiceSchema} = require('../validation/service_request');
const ServiceDocument =  require('../Model/service_request_document');
var ObjectId = require("mongodb").ObjectId;
exports.testservice = async function(req,res){
    console.log(req.files, req.body)
}

exports.create = async function(req,res){
  //console.log(req.files[0].filename,'please check')
    const result = ServiceSchema.validate(req.body);
    if(result.error){
        return res.json({
            status:false,
            message: result.error.details[0].message
        })
    }

    for(let i=0;i<req.files.length;i++){
        let file_upload = new ServiceDocument({
            name:req.files[i].filename,
            ias_user_id:req.user.id
        })
        file_upload.save(function(err){
            if(err){
                return res.json({
                    status:false,
                    message:err
                })
            }
        })
    }

    let service_request = new ServiceRequest({
        designation:req.body.designation,
        status:req.body.status, 
        district_id:req.body.district_id, 
        mobile:req.body.mobile, 
        email:req.body.email, 
        request_priority:req.body.request_priority,
        request_date:req.body.request_date,
        department_id:req.body.department_id,
        application_status:req.body.application_status,
        description:req.body.description,
        subject:req.body.subject,
        due_date:req.body.due_date,
        is_draft:req.body.is_draft,
        ias_user_id:req.user.id,
     
    })
    service_request.save(function(err){
        if(err){
            return res.json({
                status:false,
                message:error
            })
        }else{
            return res.json({
                status:false,
                message:'success'
            })
        }
    })
    
}

exports.list = async function(req,res){
    //console.log(req.params.id);
    var user_id = {_id: ObjectId(req.params.id) };
    ServiceRequest.find({ias_user_id: user_id}, function(err, result) {
        if (err) {
            return res.json({
                status:false,
                message:err
            });

        } else {
            return res.json({
                status:true,
                message:'service request',
                data:result
            });
        }
      });
}

exports.remove = async function(req,res){
    ServiceRequest.findByIdAndDelete(req.params.id,function(err){
        if(err) return next(err);
        return res.json({
                   message:'Service detail deleted Successfully'
                })
    })
}

