const { authSchema } = require('../validation/student.validation');
const MyStudents = require('../Models/students.model');
// MyStudents is behave like class and it should be camel case

exports.test = function(req,res){
   return res.send('this is my new request');
}

exports.student_create = function(req,res){
   
   // if(req.body.name.length == 0){
   //    return res.json({
   //       status:false,
   //       message:'please enter all field',
   //    })
   // }

   const stud = async() =>{
   const result =  authSchema.validate(req.body);
   if(result.error){
      return res.json({
         status:false,
         message:result.error.details[0].message
      })
   }
  //  console.log("my result 23",result.error.details[0].message);

     
   let student = new MyStudents({   
      name:req.body.name,
      age:req.body.age,
      section:req.body.section
   });
  
   student.save(function(err){
      if(err){
         // console.log("my error 12",err);  
         return res.json({
            status:false,
            error:err
         })
      }
      
     return res.json({status:true,
                     message:'Data Send Successfully',
                     data:student});
   })

   }
   stud();
};


exports.student_detail = function(req, res) {
   MyStudents.findById(req.params.id, function(err, student){

      if(err) return next(err);
     return res.json({
         status:true,
         message:"students detail",
         data:student
      })
   })
  
};

exports.student_detail_update = function(req, res){
   MyStudents.findByIdAndUpdate(req.params.id,{$set:req.body},
      function(err,student){
         if(err) return next(err);
         return res.json({
            status: true,
            message: "student detail updted successfully",
            data:student
         })
      });
 

}

exports.student_detail_delete = function(req,res){
   MyStudents.findByIdAndDelete(req.params.id, function(err,mydetail){
      if(err) return next(err);

      return res.json({
         message:'students detail deleted Successfully'
      })

   });

  
}

