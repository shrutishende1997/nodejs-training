const Joi = require('@hapi/joi')

const authSchema = Joi.object({
    name: Joi.string().required(),
    age: Joi.string().required(),
    section:Joi.string().required(),
})

module.exports= {
    authSchema
}
