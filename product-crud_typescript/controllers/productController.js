"use strict";
exports.__esModule = true;
var product = require('../model/product.model');
var getTest = function (req, res) {
    console.log('test');
};
var create = function (req, res) {
    var products = new product({
        title: req.body.title,
        price: req.body.price
    });
    products.save(function (err) {
        if (err) {
            return res.json({
                status: false,
                message: err
            });
        }
        return res.json({
            status: true,
            message: 'prduct detail added successfully',
            data: products
        });
    });
};
var remove = function (req, res) {
    product.findByIdAndRemove(req.params.id, function (err, result) {
        if (err) {
            return res.json({
                status: false,
                message: err
            });
        }
        return res.json({
            status: true,
            message: 'product deleted successfully'
        });
    });
};
var edit = function (req, res) {
    product.findByIdAndUpdate(req.params.id, { $set: req.body }, function (err) {
        if (err) {
            return res.json({
                status: false,
                message: 'product detail not updated'
            });
        }
        return res.json({
            status: true,
            message: 'product detail updated successfully'
        });
    });
};
exports["default"] = { getTest: getTest, create: create, remove: remove, edit: edit };
