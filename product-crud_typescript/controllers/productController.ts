const product = require('../model/product.model');
const getTest = function (req:any, res:any){
    console.log('test');
}
const create = function (req:any, res:any){
    let products = new product({
        title:req.body.title,
        price:req.body.price
    });
    products.save(function(err:any){
        if(err){
            return res.json({
                status:false,
                message:err
            })
        }
        return res.json({
            status:true,
            message:'prduct detail added successfully',
            data:products
        })
    })
}
const remove = function(req:any, res:any){
   product.findByIdAndRemove(req.params.id,function(err,result){
       if(err){
            return res.json({
                status:false,
                message:err
            })           
       }
       return res.json({
           status:true,
           message:'product deleted successfully'
       });
   })
}

const edit = function(req:any,res:any){
    product.findByIdAndUpdate(req.params.id,{$set:req.body},function(err){
        if(err){
            return res.json({
                status:false,
                message:'product detail not updated'
            })
        }
        return res.json({
            status:true,
            message:'product detail updated successfully'
        })
    })
}

export default {getTest,create,remove,edit};