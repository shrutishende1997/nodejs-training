"use strict";
var express1 = require('express');
var router = express1.Router();
var productController_1 = require("../controllers/productController");
router.get('/myTest', productController_1["default"].getTest);
router.post('/create', productController_1["default"].create);
router["delete"]('/:id/remove', productController_1["default"].remove);
router.put('/:id/edit', productController_1["default"].edit);
module.exports = router;
