const express1 = require('express');
const router = express1.Router();
import product_controller from '../controllers/productController';

router.get('/myTest',product_controller.getTest);
router.post('/create',product_controller.create);
router.delete('/:id/remove',product_controller.remove);
router.put('/:id/edit',product_controller.edit);


export = router;