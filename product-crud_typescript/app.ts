const routes =   require('./routes/product.route');

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require("mongoose");
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/product',routes);

const PORT: number = 5000;
mongoose.connect('mongodb://localhost:27017/products-typescript').then(()=> console.log("connection succesfully..."))
.catch((err)=> console.log(err));
app.listen(PORT, () => {
    console.log('Server is up and running on port numner ' + PORT);
});

