const mongoose =  require('mongoose');
import { Schema, model } from 'mongoose';

let ProductSchema = new Schema({
    title: {type: String, required: true, max: 100},
    price: {type: Number, required: true},
});

module.exports = mongoose.model('Product', ProductSchema);