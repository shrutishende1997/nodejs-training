"use strict";
exports.__esModule = true;
var mongoose = require('mongoose');
var mongoose_1 = require("mongoose");
var ProductSchema = new mongoose_1.Schema({
    title: { type: String, required: true, max: 100 },
    price: { type: Number, required: true }
});
module.exports = mongoose.model('Product', ProductSchema);
