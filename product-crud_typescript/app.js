var routes = require('./routes/product.route');
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require("mongoose");
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/product', routes);
var PORT = 5000;
mongoose.connect('mongodb://localhost:27017/products-typescript').then(function () { return console.log("connection succesfully..."); })["catch"](function (err) { return console.log(err); });
app.listen(PORT, function () {
    console.log('Server is up and running on port numner ' + PORT);
});
